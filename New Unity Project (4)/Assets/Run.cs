﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Run : MonoBehaviour
{
    public ParticleSystem Particle;
    public ParticleSystem Particle2;
    Vector3 x;
    Vector3 y;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        x.x = Input.GetAxis("Horizontal")*Time.deltaTime;
        y.y = Input.GetAxis("Vertical")*Time.deltaTime;
        gameObject.transform.position += x;
        gameObject.transform.position += y;
    }
 

    private void OnParticleCollision(GameObject other)
    {
        Particle.Stop();
        var heading = gameObject.transform.position - Particle.transform.position;
        Particle.GetComponent<ParticleSystemRenderer>().lengthScale = heading.magnitude;
        Particle.transform.LookAt(gameObject.transform);
        Particle.transform.Rotate(new Vector3(0, 180, 0));
        Particle.Play();
    }


    private void OnMouseDown()
    {
        Particle2.Play();
    }
}
